

import React from 'react';
import Main from './components/MainComponent';
import { NavigationContainer } from '@react-navigation/native';

 class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Main />
      </NavigationContainer>
      
    );
  }
}

export default App;
