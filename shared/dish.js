export const DISHES =
    [
        {
        id: 0,
        name:'Health & Relax',
        image: 'images/relax',
        category: 'mains',
        label:'Hot',
        price:'4.99',
        featured: true,
        description:'this is a part to show you some info about why you need to get rid of stress'                    
        },
        {
        id: 1,
        name:'How to relax',
        image: 'images/relax',
        category: 'appetizer',
        label:'',
        price:'1.99',
        featured: false,
        description:'This is a part to show you some info to let you relaxed, would use some link further'
        },
        {
        id: 2,
        name:'Healthy life',
        image: 'images/relax',
        category: 'appetizer',
        label:'New',
        price:'1.99',
        featured: false,
        description:'some tips about healthy life and happy life'
        },
        {
        id: 3,
        name:'Git rid of Stress',
        image: 'images/relax',
        category: 'dessert',
        label:'',
        price:'2.99',
        featured: false,
        description:'uselful method about get rid of stress '
        }
    ];