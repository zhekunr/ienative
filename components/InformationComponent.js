import React, { Component } from 'react';
import { View, Text } from 'react-native';

class Information extends Component {

    static navigationOptions = {
        title: 'Information'
    };

    render() {
        return(
            <View><Text>Information Component</Text></View>
        );
    }
}

export default Information;