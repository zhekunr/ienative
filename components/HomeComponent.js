import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { ListItem } from 'react-native-elements';
import { DISHES } from '../shared/dish';

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dishes: DISHES
        };
    }

    static navigationOptions = {
        title: 'Home'
    };

    render() {

        const {navigate}= this.props.navigation;

        const renderHomeItem = ({ item, index }) => {

            return (
                <ListItem
                    key={index}
                    title={item.name}
                    subtitle={item.description}
                    hideChevron={true}
                    onPress={() => navigate('Dishdetail',{dishID:item.id})}
                    leftAvatar={{ source: require('../images/relax.jpg') }}
                />
            );
        };

        return (
            <FlatList
                data={this.state.dishes}
                renderItem={renderHomeItem}
                keyExtractor={item => item.id.toString()}
            />
        );
    }

}


export default Home;