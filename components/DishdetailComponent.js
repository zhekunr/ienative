import React,{Component} from 'react';
import { Text, View } from 'react-native';
import { Card } from 'react-native-elements';
import { DISHES } from '../shared/dish';

function RenderDish(props) {

    const dish = props.dish;
    
        if (dish != null) {
            return(
                <Card
                featuredTitle={dish.name}
                image={require('../images/relax.jpg')}>
                    <Text style={{margin: 10}}>
                        {dish.description}
                    </Text>
                </Card>
            );
        }
        else {
            return(<View></View>);
        }
}

class Dishdetail extends Component{
    constructor(props){
        super(props);
        this.state={
            dishes:DISHES
        };
    }
    static navigationOptions ={
        title:'Dish Details'
    };
    render(){
        const dishID= this.props.route.params.dishID;
        return(<RenderDish dish={this.state.dishes[+dishID]} />);
    }
}

export default Dishdetail;