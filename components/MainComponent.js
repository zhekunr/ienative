

import React, { Component } from 'react';
import Home from './HomeComponent';
import Dishdetail from './DishdetailComponent';
import Diagnose from './DiagnoseComponent';
import { View, Platform } from 'react-native';
import { DISHES } from '../shared/dish';
import About from './AboutComponent';
import Information from './InformationComponent';

import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Constants from 'expo-constants';



const Stack = createStackNavigator();
function HomeNavigator() {
    return (
        <Stack.Navigator
            initialRouteName="Home"
            screenOptions={{headerStyle:{backgroundColor:"#512DA"},headerTintColor:'#512DA',headerTitleStyle:{color:"#512DA"}} }
        >
            <Stack.Screen 
            name="Home"
            component={Home}
            options={{title:'Home'}}/>
            <Stack.Screen 
            name="Dishdetail"
            component={Dishdetail}/>
        </Stack.Navigator >
    )
}
function DiagnoseNavigator(){
    return(
    <Stack.Navigator
        screenOptions={{headerStyle:{backgroundColor:"#512DA"},headerTintColor:'#512DA',headerTitleStyle:{color:"#512DA"}} }
        >
            <Stack.Screen
            name="Diagnose"
            component={Diagnose}
            options={{title:'Diagnose'}}/>
    </Stack.Navigator>
    )
}


const Drawer = createDrawerNavigator();

function MainNavigator() {
  return (
    <Drawer.Navigator backgroundColor='#D1C4E9'>
        <Drawer.Screen name="Home" component={HomeNavigator} options={{title:'Home',drawerLabel:'Home'}}/>
        <Drawer.Screen name="Diagnose" component={DiagnoseNavigator} options={{title:'Diagnose',drawerLabel:'Diagnose'}} />
        <Drawer.Screen name="Information" component={Information} options={{title:'Information',drawerLabel:'Information'}}/>
        <Drawer.Screen name="About" component={About} options={{title:'About',drawerLabel:'About'}}/>
    </Drawer.Navigator>
  );
}


class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dishes: DISHES,
            selectedDish: null
        };


    }



    render() {
        return (
            <View style={{ flex: 1, paddingTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight }}>
                <MainNavigator />
            </View>
        );
    }
}

export default Main;